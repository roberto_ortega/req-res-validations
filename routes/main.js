const express = require('express');
const router = express.Router();
const db = require('../db/db');

router.get('/api/:something', (req, res)=> {
  res.send(req.params.something)
})

router.get('/clowns', (req, res) => {
  knex('item').select().then(items => {
    res.json(items);
  });
});

router.post('/clowns', (request, response, next) => {
  if (request.body.name.length < 3 || !request.body.description) {
    response.send("Invalid info")
  } else {
    let clown = {
      name: request.body.name,
      description: request.body.description
    }
    db.insert(clown).then(() => {
      response.send("all went well")
    })
  }
})

// https://learn.galvanize.com/cohorts/89/daily_plans/2016-12-12

router.get('/cohorts/:apples/daily_plans/:date', (req, res) => {
  console.log(req.params);
  //{apples: 89, date: 2016-12-12}
  res.send("example")
})

router.get('/clowns/:clown_id', (req, res) => {
  if (typeof Number(req.params.clown_id) != "number") {
    res.send("dude, id is not a number")
  } else {
    db.where(req.params.clown_id).then(clown => {
      res.header('content-type', 'application-json')
      res.send(clown)
    })
  }
})

module.exports = router;
