const knex = require('./knex');

module.exports = {
  insert: function (clown) {
    return knex('item').insert(clown);
  },
  where: function (id) {
    return knex('item').where("id", id);
  }
}
